﻿using System;

namespace FirstTask
{
    public class Printer
    {
        public static void PrintFirstCharToConsole(string word)
        {
            Console.WriteLine(word[0]);
        }
    }
}

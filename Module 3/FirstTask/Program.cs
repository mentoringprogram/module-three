﻿using System;

namespace FirstTask
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var input = "";

            while (input != "q")
            {
                Console.WriteLine("Print your line or press \"q\" to quit");
                input = Console.ReadLine();

                if (input != null && input.Equals("q"))
                {
                    break;
                }

                try
                {
                    Printer.PrintFirstCharToConsole(input);
                }
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine("The input word is empty.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Something went wrong.");
                }
            }

            Console.WriteLine("The program has finished");

        }
    }
}

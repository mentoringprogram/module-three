﻿using System;
using System.Linq;

namespace SecondTask
{
    public class Parser
    {
        private const int MinUnicodeValue = 48;
        private const int MaxUnicodeValue = 57;

        public static bool TryParse(string s, out int number)
        {
            if (string.IsNullOrEmpty(s))
            {
                number = 0;
                return false;
            }

            //Remove sign 'minus' to get only numbers from the string
            var isNegative = false;
            if (s[0] == '-')
            {
                isNegative = true;
                s = s.Remove(0, 1);
            }

            //Revers array for cycle
            var charNumbers = s.ToCharArray().Reverse().ToArray();

            if (!CheckIsNumber(charNumbers))
            {
                number = 0;
                return false;
            }

            var num = 0;

            //Convert chars array to int number
            //'checked' need for overflow control
            for (var i = 0; i < charNumbers.Length; i++)
            {
                int temp = charNumbers[i];
                checked
                {
                    num += (temp - MinUnicodeValue) * (int) Math.Pow(10, i);
                }
            }

            //If strings number was negative then get negative int number
            number = isNegative ? num * (-1) : num;
            return true;
        }

        //For unicode cheking
        private static bool CheckIsNumber(char[] charNumbers)
        {
            if (charNumbers == null)
            {
                throw new ArgumentNullException(nameof(charNumbers));
            }

            return charNumbers.All(temp => temp >= MinUnicodeValue && temp <= MaxUnicodeValue);
        }
    }
}
